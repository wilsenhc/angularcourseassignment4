import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {
  @Output() numberIncremented = new EventEmitter<number>();
  counter: number;

  intervalRef: any;

  constructor() { }

  ngOnInit() {
    this.counter = 0;
  }

  onStartGame() {
    this.counter = 0;
    this.intervalRef = setInterval(() => {
      this.counter += 1;
      this.numberIncremented.emit(this.counter);
    }, 1000);
  }


  onStopGame() {
    clearInterval(this.intervalRef);
  }

}
